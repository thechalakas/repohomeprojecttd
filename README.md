# repohomeprojecttd

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to repos related to project TD.

**here are the repos**

I have written some code related to the now closed project called project TD. it was meant to use alexa heavily but I was not comfortable with some of amazon policies so I stopped this project and switching the AI backend to something else. 

---

1. [https://bitbucket.org/thechalakas/projecttd_webapi_attempt1]

2. [https://bitbucket.org/thechalakas/projecttd_webapi_attempt2]

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 